import React, {Component} from 'react'
import WOW from 'wow.js'

class Works extends Component {
	
	componentDidMount() {
		let wow = new WOW(
			{
				animateClass: 'animated',
				offset: 100
			}
		);
		wow.init();
	}
	
	render() {
		return (
			<section className="main-section paddind" id="Portfolio">
				<div className="container">
					<h2>Мои работы</h2>
					<h6>Недавние работы подробнее раскажут о моих навыках </h6>
					<div className="portfolioFilter">
						<ul className="Portfolio-nav wow fadeIn delay-02s">
							<li><a role="button" data-filter="*" className="current">Все</a></li>
							<li><a role="button" data-filter=".Landing">Landing Page</a></li>
							<li><a role="button" data-filter=".adaptive">Адаптивная верстка</a></li>
							<li><a role="button" data-filter=".Site">Многостраничные сайты</a></li>
						</ul>
					</div>
				</div>
				<div className="portfolioContainer wow fadeInUp delay-04s">
					<div className=" Portfolio-box Site adaptive">
						<a role="button" target="_blank" rel="noopener noreferrer"><img src="img/Portfolio-pic10.jpg"
						                                                                alt=""/></a>
						<h3>Грин Мед</h3>
						<p>Интернет магазин</p>
					</div>
					
					<div className="Portfolio-box Landing">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img
							src="img/Portfolio-pic11.jpg" alt=""/></a>
						<h3>Nimp Agency</h3>
						<p>Landing не адаптивный</p>
					</div>
					
					<div className=" Portfolio-box Landing adaptive">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img
							src="img/Portfolio-pic3.jpg" alt=""/></a>
						<h3>Brandi</h3>
						<p>Landing адаптивный до 780px</p>
					</div>
					
					<div className=" Portfolio-box Sait">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img
							src="img/Portfolio-pic2.jpg"
							alt=""/></a>
						<h3>Конкурс Экспобанка</h3>
						<p>Многостраничный сайт</p>
					</div>
					
					<div className=" Portfolio-box Landing">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img
							src="img/Portfolio-pic5.jpg" alt=""/></a>
						<h3>News Rerporter</h3>
						<p>Landing</p>
					</div>
					
					<div className=" Portfolio-box Landing adaptive">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img
							src="img/Portfolio-pic1.jpg"
							alt=""/></a>
						<h3>Лендинг</h3>
						<p>Лендинг игрушек</p>
					</div>
					
					<div className=" Portfolio-box Landing">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img
							src="img/Portfolio-pic7.jpg" alt=""/></a>
						<h3>Active Kurulush</h3>
						<p>Landing</p>
					</div>
					
					
					<div className=" Portfolio-box Landing">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img
							src="img/Portfolio-pic9.jpg" alt=""/></a>
						<h3>Threezine</h3>
						<p>Landing</p>
					</div>
					
					<div className="Portfolio-box Landing">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img
							src="img/Portfolio-pic8.jpg" alt=""/></a>
						<h3>Сайт Стекломонтаж-24</h3>
						<p>Landing</p>
					</div>
					
					<div className="Portfolio-box Landing">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img
							src="img/israel_landing.jpg"
							alt=""/></a>
						<h3>Израильский симфонический оркестр</h3>
						<p>Landing Page</p>
					</div>
					
					<div className=" Portfolio-box Site adaptive">
						<a href="http://www.rabdee.ru" target="_blank" rel="noopener noreferrer"><img src="img/life.jpg"
						                                                                              alt=""/></a>
						<h3>ЖИЗНЬ БЕЗ ДЕПРЕССИИ</h3>
						<p>Адаптивный сайт</p>
					</div>
				</div>
			</section>
		
		)
	}
}

export default Works;