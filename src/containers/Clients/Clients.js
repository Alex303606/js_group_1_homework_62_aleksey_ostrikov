import React,{Component} from 'react'
import WOW from "wow.js";

class Clients extends Component {
	componentDidMount() {
		let wow = new WOW(
			{
				animateClass: 'animated',
				offset: 100
			}
		);
		wow.init();
	}
	
	render(){
		return (
			<section className="main-section client-part" id="client">
				<div className="container">
					<b className="quote-right wow fadeInDown delay-03"><i className="fa fa-quote-right"></i></b>
					<div className="row">
						<div className="col-lg-12">
							<p className="client-part-haead wow fadeInDown delay-05">
								Хочу выразить благодарность Алексею за качественную и оперативную верстку. Все работы выполнены в срок, согласно ТЗ. Правки вносились очень быстро, кроме того, их было не много. <br/> Очень хороший специалист.</p>
						</div>
					</div>
					<ul className="client wow fadeIn delay-05s">
						<li>
							<div className="client_block">
								<img src="img/client-pic1.jpg" alt=""/>
								<h3>Александр Иванов</h3>
								<span>License To Drink Inc.</span>
							</div>
						</li>
					</ul>
				</div>
			</section>
		)
	}
}

export default Clients;