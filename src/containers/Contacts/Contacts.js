import React,{Component} from 'react'
import WOW from "wow.js";

class Contacts extends Component {
	componentDidMount() {
		let wow = new WOW(
			{
				animateClass: 'animated',
				offset: 100
			}
		);
		wow.init();
	}
	render(){
		return (
			<div className="container">
				<section className="main-section contact" id="contact">
					<div className="row">
						<div className="col-lg-6 col-sm-7 wow fadeInLeft">
							<div className="contact-info-box address clearfix">
								<h3><i className="fa fa-skype"></i>Skype:</h3>
								<span>alexey_ostrikov</span>
							</div>
							<div className="contact-info-box phone clearfix">
								<h3><i className="fa-phone"></i>Телефон:</h3>
								<span>+996-555-303-606</span>
							</div>
							<div className="contact-info-box email clearfix">
								<h3><i className="fa fa-envelope"></i>email:</h3>
								<span>alex303606@gmail.com</span>
							</div>
							
							<ul className="social-link">
								<li className="facebook"><a href="https://www.facebook.com/profile.php?id=100003530073345"
								                            rel="noopener noreferrer" target="_blank"><i className="fa fa-facebook"></i></a></li>
								<li className="skype"><a href="skype:alexey_ostrikov"><i className="fa fa-skype"></i></a>
								</li>
								<li className="gplus"><a rel="noopener noreferrer" href="https://plus.google.com/u/0/117000032364628168512"
								                         target="_blank"><i className="fa fa-google-plus"></i></a></li>
								<li className="phone"><a href="tel:+996-555-303-606"><i className="fa fa-phone"></i></a>
								</li>
							</ul>
						</div>
						<div className="col-lg-6 col-sm-5 wow fadeInUp delay-05s">
							<div className="form">
								<div className="divError" id="error"></div>
								<form action='no-script-url: 0' name="myform" id="myform">
									<input className="input-text vvod" required="required" type="text" name="name"
									       placeholder="Ваше имя *"/>
									<input className="input-text vvod" required="required" type="text" name="email"
									       placeholder="Ваш E-mail *"/>
									<textarea name="mess" required="required"
									          className="input-text text-area vvod_textarea"
									          placeholder="Ваше сообщение *"></textarea>
									<input className="input-btn" type="submit" value="Отправить"/>
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
		)
	}
}

export default Contacts;