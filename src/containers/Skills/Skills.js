import React, {Component} from 'react'
import WOW from "wow.js";


class Skills extends Component {
	
	componentDidMount() {
		let wow = new WOW(
			{
				animateClass: 'animated',
				offset: 100
			}
		);
		wow.init();
	}
	
	render() {
		return (
			<section className="main-section" id="service">
				<div className="container">
					<h2>Мои навыки</h2>
					<h6>Как <strong> HTML &ndash; верстальщик</strong> я умею, пожалуй, все.
						Как <strong>Front-End</strong> разработчик - не все, но многое, а именно:</h6>
					<div className="row">
						<div className="service-lists col-lg-4 col-sm-6 wow fadeInLeft delay-05s">
							<div className="service-list">
								<div className="service-list-col1">
									<i className="fa-thumbs-up"></i>
								</div>
								<div className="service-list-col2">
									<h3>валидный код</h3>
									<p>Код HTML и CSS не противоречащий W3C и соответствующий проверке
										валидатором...</p>
								</div>
							</div>
							<div className="service-list">
								<div className="service-list-col1">
									<i className="icon-responsive my_icon">&nbsp;</i>
								</div>
								<div className="service-list-col2">
									<h3>Адаптивность</h3>
									<p>Обеспечивает правильное отображение сайта на различных устройствах</p>
								</div>
							</div>
							<div className="service-list">
								<div className="service-list-col1">
									<i className="icon-browsers my_icon">&nbsp;</i>
								</div>
								<div className="service-list-col2">
									<h3>Кроссбраузерность</h3>
									<p>Cвойство сайта отображаться и работать во всех популярных браузерах идентично</p>
								</div>
							</div>
							<div className="service-list">
								<div className="service-list-col1">
									<i className="fa-th"></i>
								</div>
								<div className="service-list-col2">
									<h3>Pixel perfect</h3>
									<p>100% соответствие макету</p>
								</div>
							</div>
						</div>
						<figure className="col-lg-8 col-sm-6  text-right wow fadeInUp delay-02s">
							<img src="img/iphone.png" height="462" width="521" alt=""/>
						</figure>
					</div>
				</div>
			</section>
		)
	}
}

export default Skills;