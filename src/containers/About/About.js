import React,{Component} from 'react'
import certificate from '../../images/certificate.jpg'
class About extends Component {
	render(){
		return (
			<section id="iAm" className="main-section alabaster">
				<div className="container">
					<div className="row">
						<figure className="col-lg-3 col-lg-offset-1 col-sm-4 wow fadeInLeft">
							<div className="team-leader-box">
								<div className="team-leader wow fadeInDown delay-03s">
									<div className="team-leader-shadow"></div>
									<img src="img/team-leader-pic2.jpg" alt=""/>
									<ul>
										<li>
											<a href="skype:alexey_ostrikov" className="fa fa-skype"><i></i></a>
										</li>
										<li>
											<a rel="noopener noreferrer" href="https://www.facebook.com/profile.php?id=100003530073345" className="fa-facebook" target="_blank"><i></i></a>
										</li>
										<li>
											<a rel="noopener noreferrer" href="https://plus.google.com/u/0/117000032364628168512" className="fa-google-plus" target="_blank"><i></i></a>
										</li>
									</ul>
								</div>
								<h3 className="wow fadeInDown delay-03s">Остриков Алексей</h3>
								<span className="wow fadeInDown delay-03s">HTML верстальщик</span>
								<p className="wow fadeInDown delay-03s">Занимаюсь версткой сайтов, для меня это не просто хобби, а основной заработок и поэтому к работе отношусь серьезно и с большой ответственностью.</p>
							</div>
							<div className="certificate">
								<a className="fancybox" href={certificate} title="Сертификат">
									<img src={certificate} height="3006" width="4205" alt="Сертификат"/>
								</a>
							</div>
						</figure>
						<div className="col-lg-7 col-lg-offset-1 col-sm-7 col-sm-offset-1 featured-work">
							<h2>Что я знаю и умею:</h2>
							<p className="padding-b">Обладая необходимым опытом, предлагаю вам себя на позицию <strong>"HTML-верстальщик"</strong> или <strong>"Front-End разработчик"</strong>. Я знаю и умею работать со следующики технологиями и инструментами:</p>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-02s">
									<i className="fa fa-html5"></i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-02s">
									<h3>Код разметки HTML 5</h3>
									<p>Семантическая кроссбраусерная валидная вёрстка. Sublime Text 2. Плагины Emmet, AutoFileName.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-04s">
									<i className="fa fa-css3"></i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-04s">
									<h3>Таблицы стилей CSS 3</h3>
									<p>Иерархия селекторов. Блочная модель. Применение CSS-анимаций. Создание параллакс эффектов на CSS.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-06s">
									<i className="icon-photoshop my_icon">&nbsp;</i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-06s">
									<h3>Adobe Photoshop</h3>
									<p>Нарезка макета. Подготовка изображений для Web. Применение спрайтов. Работа с SVG-графикой.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-02s">
									<i className="fa fa-gear"></i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-02s">
									<h3>Интерактив</h3>
									<p>Подключение готовых скриптов. Вёрстка табов, слайдеров, всплывающих окон. Настройка Slick, Revolution, Owl, Fancybox.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-04s">
									<i className="icon-responsive my_icon">&nbsp;</i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-04s">
									<h3>Адаптивная верстка</h3>
									<p>Принципы адаптивной верстки (@media queries, %, em).Верстка под iOS и мобильную Safari. Раскладка с помощью Flexible Box.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-06s">
									<i className="icon-jquery my_icon">&nbsp;</i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-06s">
									<h3>Библиотека JQuery</h3>
									<p>Введение в JQuery. Свойства объекта. События и эффекты jQuery. Работа с Document Object Model. Написание простых скриптов на jQuery.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-02s">
									<i className="icon-bootstrap my_icon">&nbsp;</i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-02s">
									<h3>Фреймворк Bootstrap</h3>
									<p>Преимущества и недостатки CSS-фреймвоков. Верстка страницы с использованием Twitter Bootstrap. Фреймворки для построения сетки.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-04s">
									<i className="fa fa-exclamation-triangle"></i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-04s">
									<h3>Важное</h3>
									<p>Принципы БЭМ (Блок - Элемент - Модификатор). Кастомизация элементов формы (form styler). Валидация формы на клиенте. Нестандартные шрифты.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-06s">
									<i className="fa fa-git"></i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-06s">
									<h3>Git / Bitbucket</h3>
									<p>Системы контроля версий. Создание и клонирование репозитория. Внесение изменений в репозиторий (pull, add, commit, push). Совместная работа над проектом.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-02s">
									<i className="icon-sass my_icon">&nbsp;</i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-02s">
									<h3>Пре-процессор SASS</h3>
									<p>Наследование. Миксины (Bourbon). Математические операторы. Интерполяция переменных. Циклы @for и @each. Шаблонные селекторы.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-04s">
									<i className="icon-gulp my_icon">&nbsp;</i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-04s">
									<h3>Сборщик Gulp</h3>
									<p>Знакомство с Gulp. Установка и настройка. Использование npm и node.js для подключения плагинов. Автоматизация сборки макетов при верстке крупных проектов.</p>
								</div>
							</div>
							<div className="featured-box col-lg-6">
								<div className="featured-box-col1 wow fadeInRight delay-06s">
									<i className="fa fa-plus-square"></i>
								</div>
								<div className="featured-box-col2 wow fadeInRight delay-06s">
									<h3>Прочее</h3>
									<p>Иконочный шрифт (Font Awesome). Псевдо-классы и псевдо-элементы. Вёрстка тултипов. Работа с хостингом. Локальный веб-сервер (Open Server). FTP. Основы работы с командной строкой.</p>
								</div>
							</div>
							<a className="Learn-More" href="/">Learn More</a>
						</div>
					</div>
				</div>
			</section>
		)
	}
}

export default About;