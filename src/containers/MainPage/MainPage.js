import React, {Component} from 'react'
import WOW from "wow.js";

class MainPage extends Component {
	
	goToAbout = (e) => {
		e.preventDefault();
		this.props.history.push('/about');
	};
	
	componentDidMount() {
		let wow = new WOW(
			{
				animateClass: 'animated',
				offset: 100
			}
		);
		wow.init();
	}
	
	render() {
		return (
			<div className="header" id="header">
				<div className="container">
					<h1 className="animated fadeInDown delay-20s">front-end developer</h1>
					<ul className="we-create animated fadeInUp delay-30s">
						<li>html&ndash; верстальщик</li>
					</ul>
					<div className="animated fadeInUp delay-30s">
						<a onClick={(e) => this.goToAbout(e)} className="link mouse" href="">Обо мне</a>
					</div>
				</div>
			</div>
		)
	}
}

export default MainPage;