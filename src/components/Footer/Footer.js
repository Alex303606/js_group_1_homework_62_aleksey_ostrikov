import React from 'react'

const Footer = props => {
	return (
		<footer className="footer">
			<div className="container">
				<div className="footer-logo">
					<a href="" className="cssload-loader cssload-loader-small">
						<i className="cssload-inner cssload-one"></i>
						<i className="cssload-inner cssload-two"></i>
						<i className="cssload-inner cssload-three"></i>
					</a>
				</div>
				<span className="copyright">Copyright &copy; 2018 | <a href="">WWW.HTML.GO.KG</a> <br/> by Ostrikov Aleksey.</span>
			</div>
		</footer>
	)
};

export default Footer;
