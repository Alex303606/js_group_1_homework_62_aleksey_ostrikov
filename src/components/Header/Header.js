import React,{Component} from 'react'
import {NavLink} from "react-router-dom";

class Header extends Component {
	render(){
		return (
			<nav className="main-nav-outer" id="test">
				<div className="container">
					<ul className="main-nav">
						<li><NavLink exact activeClassName="selected" to="/">Главная</NavLink></li>
						<li><NavLink activeClassName="selected" to="/about">Обо мне</NavLink></li>
						<li><NavLink activeClassName="selected" to="/works">Мой работы</NavLink></li>
						<li className="small-logo"><a href="#header">
							<div className="cssload-loader cssload-loader-small">
								<div className="cssload-inner cssload-one"></div>
								<div className="cssload-inner cssload-two"></div>
								<div className="cssload-inner cssload-three"></div>
							</div>
						</a></li>
						<li><NavLink activeClassName="selected" to="/clients">Отзывы клиентов</NavLink></li>
						<li><NavLink activeClassName="selected" to="/skills">Мои навыки</NavLink></li>
						<li><NavLink activeClassName="selected" to="/contacts">Контакты</NavLink></li>
					</ul>
					<a className="res-nav_click" role="button"><i className="fa-bars"></i></a>
				</div>
			</nav>
		)
	}
}

export default Header;