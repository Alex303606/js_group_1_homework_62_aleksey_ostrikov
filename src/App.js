import React, {Component, Fragment} from 'react';
import MainPage from "./containers/MainPage/MainPage";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import About from "./containers/About/About";
import {Route, Switch} from "react-router-dom";
import Works from "./containers/Works/Works";
import Clients from "./containers/Clients/Clients";
import Skills from "./containers/Skills/Skills";
import Contacts from "./containers/Contacts/Contacts";

class App extends Component {
	render() {
		return (
			<Fragment>
				<Header/>
				<Switch>
					<Route path="/" exact component={MainPage}/>
					<Route path="/About" component={About}/>
					<Route path="/Works" component={Works}/>
					<Route path="/Clients" component={Clients}/>
					<Route path="/Skills" component={Skills}/>
					<Route path="/Contacts" component={Contacts}/>
					<Route render={()=> <h1>404 not found</h1>}/>
				</Switch>
				<Footer/>
			</Fragment>
		);
	}
}

export default App;
