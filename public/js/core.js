$(document).ready(function () {
	
	$('.fancybox').fancybox();
	
	$('#test').scrollToFixed();
	
	$('.res-nav_click').click(function () {
		$('.main-nav').slideToggle();
		return false
	});
});

$(window).load(function(){  
  
  let $container = $('.portfolioContainer'),
      $body = $('body'),
      colW = 375,
      columns = null;
  
  $container.isotope({
    // disable window resizing
    resizable: true,
    masonry: {
      columnWidth: colW
    }
  });
  
  $(window).smartresize(function(){
    // check if columns has changed
    let currentColumns = Math.floor( ( $body.width() -30 ) / colW );
    if ( currentColumns !== columns ) {
      // set new column count
      columns = currentColumns;
      // apply width to container manually, then trigger relayout
      $container.width( columns * colW )
        .isotope('reLayout');
    }
    
  }).smartresize(); // trigger resize to set container width
	
  $('.portfolioFilter a').click(function(){
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');
 
        let selector = $(this).attr('data-filter');
        $container.isotope({
			
            filter: selector,
         });
         return false;
    });  
});











